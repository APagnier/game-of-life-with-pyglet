from cell import Cell
from var import MAX_X, MAX_Y

class Board(object):
    def __init__(self):
        self.map = []
        for y in range(MAX_Y):
            self.map.append([])
            for x in range(MAX_X):
                self.map[y].append(Cell(x, y))

    def get_cell(self, x, y):
        return self.map[y][x]
