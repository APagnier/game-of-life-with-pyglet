This project is based on [Conway's Game of Life automaton](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

It requires python3 and pyglet.

Commands are :  
  - **left click** to change cells state (it is possible to drag)  
  - **right click** to start/stop iterations  
