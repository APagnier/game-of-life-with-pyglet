import board
import cell
from var import MAX_X, MAX_Y

import pyglet
import time

CURR_DRAG = (-1, -1)
window = pyglet.window.Window(MAX_X*10-1, MAX_Y*10-1, "Game of life")

batch = pyglet.graphics.Batch()

# defines lines for grid
lines = []
for y in range(MAX_Y+1):
    line = pyglet.shapes.Line(0, y*10-1, MAX_X*10, y*10-1, 1, color = (255, 255, 255), batch = batch)
    line.opacity = 150
    lines.append(line)
for x in range(MAX_X+1):
    line = pyglet.shapes.Line(x*10, 0, x*10, MAX_Y*10, 1, color = (255, 255, 255), batch = batch)
    line.opacity = 150
    lines.append(line)

board = board.Board()
run_loop = 0

def main_loop(dt):
    '''
    Main loop to update cells
    Args: dt: (float) delta time since last execution
    '''
    if run_loop != 0:
        for y in range(MAX_Y):
            for x in range(MAX_X):
                board.get_cell(x, y).calc_new(board)
        for y in range(MAX_Y):
            for x in range(MAX_X):
                board.get_cell(x, y).change_state()

# schedule main_loop to execute each second
pyglet.clock.schedule_interval(main_loop, 1)

@window.event
def on_mouse_press(x, y, button, modifiers):
    '''
    Change cell status on left click
    or allow main loop on right click 

    Args:
        x (int) : position of mouse from left of window
        y (int) : position of mouse from bottom of window
        button (pyglet.window.mouse) : button of mouse press
        modifiers : unused
'''
    global CURR_DRAG
    global run_loop
    if button == pyglet.window.mouse.LEFT:
        CURR_DRAG = (int(x/10), int(y/10))
        print( str(int(x/10)) + "/" + str(int(y/10)))
        board.get_cell(int(x/10), int(y/10)).switch_state()
    if button == pyglet.window.mouse.RIGHT and run_loop == 0:
        run_loop = 1
    elif button == pyglet.window.mouse.RIGHT and run_loop == 1:
        run_loop = 0

@window.event
def on_mouse_drag(x, y, dx, dy, buttons, modifiers):
    '''
    Allow to change multiple cell when the mouse is dragged
    '''
    global CURR_DRAG
    if buttons == pyglet.window.mouse.LEFT:
        if CURR_DRAG != (int(x/10), int(y/10)):
            print( str(int(x/10)) + "/" + str(int(y/10)))
            board.get_cell(int(x/10), int(y/10)).switch_state()
            CURR_DRAG = (int(x/10), int(y/10))

@window.event
def on_mouse_release(x, y, button, modifiers):
    '''
    Reset the var showing the current cell dragged
    '''
    global CURR_DRAG
    if CURR_DRAG != (-1, -1):
        CURR_DRAG = (-1, -1)

@window.event
def on_draw():
    '''
    Draw loop
    '''
    window.clear()
    cells = []
    for y in range(MAX_Y):
        for x in range(MAX_X):
            if board.get_cell(x,y).get_state() == 1:
                cell_color = (255, 255, 255)
            else:
                cell_color = (0, 0, 0)
            cell = pyglet.shapes.Rectangle(x*10, y*10, 9, 9, color=cell_color, batch=batch)
            cell.opacity = 255
            cells.append(cell)
    batch.draw()

pyglet.app.run()

