from var import MAX_X, MAX_Y

class Cell(object):
    def __init__(self, x, y):
        self._x = x
        self._y = y
        self.state = 0
        self.new_state = 0

    def switch_state(self):
        self.state = 0 if self.state == 1 else 1

    def change_state(self):
        self.state = self.new_state
        self.new_state = 0

    def get_state(self):
        return self.state

    def calc_new(self, board):
        alive_neighboor = 0
        x = self._x
        y = self._y
        if x > 0 and y > 0:
            alive_neighboor += board.get_cell(x-1, y-1).get_state()
        if x > 0:
            alive_neighboor += board.get_cell(x-1, y).get_state()
        if x > 0 and y < MAX_Y-1:
            alive_neighboor += board.get_cell(x-1, y+1).get_state()
        if y > 0:
            alive_neighboor += board.get_cell(x, y-1).get_state()
        if y < MAX_Y-1:
            alive_neighboor += board.get_cell(x, y+1).get_state()
        if x < MAX_X-1 and y > 0:
            alive_neighboor += board.get_cell(x+1, y-1).get_state()
        if x < MAX_X-1:
            alive_neighboor += board.get_cell(x+1, y).get_state()
        if x < MAX_X-1 and y < MAX_Y-1:
            alive_neighboor += board.get_cell(x+1, y+1).get_state()

        if alive_neighboor == 3:
            self.new_state = 1
        elif alive_neighboor in [2, 3] and self.state == 1:
            self.new_state = 1
        else:
            self.new_state = 0

